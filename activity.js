
let http = require("http");

http.createServer(function (request, response) {

    // 1
    if (request.url == "/" && request.method == "GET") {
        response.writeHead(200, {
            'content-type' : 'text/plain'
        });
        response.end('Welcome to booking system');
    // 2
    } else if (request.url == "/profile" && request.method == "GET") {
        response.writeHead(200, {
            'content-type' : 'text/plain'
        });
        response.end("Welcome to your profile");
    // 3
    } else if (request.url == "/courses" && request.method == "GET") {
        response.writeHead(200, {
            'content-type' : 'text/plain'
        });
        response.end("Here's our courses available");
    // 4
    } else if (request.url == "/addCourse" && request.method == "POST") {
        response.writeHead(200, {
            'content-type' : 'text/plain'
        });
        response.end("Add course to our resources");
    // 5
    } else if (request.url == "/updateCourse" && request.method == "PUT") {
        response.writeHead(200, {
            'content-type' : 'text/plain'
        });
        response.end("Update a course to a database");
    // 6
    } else if (request.url == "/archiveCourse" && request.method == "DELETE") {
        response.writeHead(200, {
            'content-type' : 'text/plain'
        });
        response.end("Archive courses to our resources");
    }

}).listen(4000)

console.log("Server is running at localhost: 4000");